# voxelgoodenough

Original textures for VoxeLibre

This is the growing collection of textures that were specifically made for the VoxeLibre projects.

License is cc0 unless noted otherwise in LICENSE
